<?php

session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set("log_errors", 1);
if (file_exists('./error.log')) {
    ini_set("error_log", "./error.log");
} else {
    ini_set("error_log", "../error.log");
}
$_SESSION['showStringIds'] = TRUE;
$_SESSION['showStringIds'] = FALSE;

if (file_exists('functions.php')) {
    include_once 'functions.php';
} else {
    include_once '../functions.php';
}

if (file_exists('./classes/Autoloader.class.php')) {
    include_once './classes/Autoloader.class.php';
} else {
    include_once '../classes/Autoloader.class.php';
}


spl_autoload_register(array('Autoloader', 'load'));
$DBH = DbConnect::getConnection();
