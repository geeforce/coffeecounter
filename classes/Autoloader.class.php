<?php

class Autoloader {

    public static function load($class) {
        $class = str_replace("\\", DIRECTORY_SEPARATOR, $class);
//        echo $class;
        if (file_exists('./models/' . $class . '.class.php')) {
            include_once './models/' . $class . '.class.php';
            return true;
        }
        if (file_exists('../models/' . $class . '.class.php')) {
            include_once '../models/' . $class . '.class.php';
            return true;
        }
        if (file_exists('./controller/' . $class . '.class.php')) {
            include_once './controller/' . $class . '.class.php';
            return true;
        }
        if (file_exists('../controller/' . $class . '.class.php')) {
            include_once '../controller/' . $class . '.class.php';
            return true;
        }
        if (file_exists('./classes/' . $class . '.class.php')) {
            include_once './classes/' . $class . '.class.php';
            return true;
        }
        if (file_exists('../classes/' . $class . '.class.php')) {
            include_once '../classes/' . $class . '.class.php';
            return true;
        }
        if (file_exists('./helper/' . $class . '.class.php')) {
            include_once './helper/' . $class . '.class.php';
            return true;
        }
        if (file_exists('../helper/' . $class . '.class.php')) {
            include_once '../helper/' . $class . '.class.php';
            return true;
        }
        if (file_exists('../classes/' . $class . '.php')) {
            include_once '../classes/' . $class . '.php';
            return true;
        }
        if (file_exists('./classes/' . $class . '.php')) {
            include_once './classes/' . $class . '.php';
            return true;
        }
        $fp = fopen('../error.log', 'a');
        fwrite($fp, $class . " not found");
        return $class . " not found";
    }

}

?>
