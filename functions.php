<?php

function arrayOut($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function arrayDetail($array) {
    echo "<pre>";
    var_dump($array);
    echo "</pre>";
}

function showDate($timestamp) {
    echo "date: " . date('d.m.Y', $timestamp);
}

function alert($alertString) {
    echo '<script type=text/javascript>alert("' . $alertString . '");</script>';
}

function dbTimestamp() {
    return date('Y-m-d H:i:s');
}

function isDev() {
    if (substr_count($_SERVER['SERVER_NAME'], 'dev') > 0) {
        return true;
    }
    return false;
}

?>
