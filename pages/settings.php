<?php
$settings = Setting::find(1);
$price = $settings->getPrice();
//arrayOut($price);
?>

<table cellpadding="5" cellspacing="5">
    <tr>
        <td align="right"><b>Kaffepreis:</b></td>
        <td align="left"><input type="text"  class="form-control price" value="<?php echo $price ?>"></td>
        <td>€</td>
    </tr>
    
    <tr>
        <td align="center" colspan="2"><button type="button" class="btn btn-primary update-settings">Submit</button></td>
    </tr>
</table>
<script>
    $(document).ready(function () {
        $(".update-settings").on("click", function () {
            var price = $(".price").val().replace(/,/g, '.');
//            alert(balance);
            var data = JSON.stringify({'price': price});
            $.ajaxSetup({cache: false});
            $.ajax({
                type: "POST",
                url: "../asyncActors/updateSettings.ajx.php",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: data,
                success: function (jsonReturn) {
//                    console.log(jsonReturn);
                    $("#check-overlay").show();
                    window.setTimeout(location.href = 'index.php?page=settings', 2500);
                },
                error: function (xhr, status, errorThrown) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                }
            });
        });
    });
</script>