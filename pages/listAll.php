<?php
$users = User::all();
$overallCoffeeNum = Booking::query("SELECT COUNT(*) as overall_coffee FROM bookings WHERE amount < 0")['overall_coffee'];
$weekCoffeeNum = Booking::query("SELECT COUNT(*) as week_coffee FROM bookings WHERE amount < 0 AND YEARWEEK(`date_time`, 1) = YEARWEEK(CURDATE(), 1)")['week_coffee'];
?>
<script>
    $(document).ready(function () {
        $('#coffeelist').DataTable({
            "paging": false,
            "order": [[1, "asc"]]
        });
    });
</script>
<div><b>Kaffee gesamt: <?php echo $overallCoffeeNum; ?></b> | <b>Diese Woche: <?php echo $weekCoffeeNum; ?></b></div>
<table id="coffeelist"  class="display" style="width:100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Kontostand</th>
            <th style="width:5%;" align="right">Edit</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($users as $user) {
            if(!empty($user->getName())){
                $name = $user->getName();
            }else{
                $name = "ID: ".$user->getTag_id();
            }
            echo '<tr>';
            echo '<td>' . $name . '</td>';
            echo '<td>' . $user->sumBookings() . ' €</td>';
            echo '<td align="right"><a href="index.php?page=details&userId=' . $user->getId() . '"><i class="fas fa-edit user-edit" style="font-size: 1.5em; cursor:pointer;"></i></a></td>';
            echo '</tr>';
        }
        ?>
    </tbody>
</table>

