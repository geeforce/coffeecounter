<?php
$userId = $_REQUEST['userId'];
$user = User::find($userId);
//arrayOut($user);
$params[] = "user_id";
$params[] = $userId;
$append = "ORDER BY id DESC";
$allBookings = Booking::where($params, $append);
?>
<table cellpadding="5" cellspacing="5">
    <tr>
        <td align="right"><b>Name:</b></td>
        <td align="left"><input type="text"  class="form-control name" value="<?php echo $user->getName(); ?>"></td>
    </tr>
    <tr>
        <td align="right"><b>Id:</b> </td>
        <td align="left"><?php echo $user->getTag_id(); ?></td>
    </tr>
    <tr>
        <td align="right"><b>Kontostand:</b> </td>
        <td align="left"><?php echo $user->sumBookings(); ?> €</td>
    </tr>
    <tr>
        <td align="right"><b>Einzahlung:</b></td>
        <td align="left"><input type="text"  class="form-control balance"></td>
    </tr>
    <tr>
        <td align="center" colspan="2"><button type="button" class="btn btn-primary update-btn">Submit</button></td>
    </tr>
</table>
<hr>    
<h2>Bookings</h2>
<table id="bookings"  class="display" style="width:100%">
    <thead>
        <tr>
            <th>Datum</th>
            <th>Preis</th>
            <th style="width:5%;" align="right">L&ouml;schen</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($allBookings as $booking) {
            echo '<tr>';
            echo '<td>' . $booking->getDate_time() . '</td>';
            echo '<td>' . $booking->getAmount() . ' €</td>';
            echo '<td align="center"><i class="far fa-trash-alt delete-booking" id="' . $booking->getId() . '" style="font-size: 1.5em; cursor:pointer; z-index:10;"></td>';
            echo '</tr>';
        }
        ?>
    </tbody>
</table>


<script>
    $(document).ready(function () {
        userId = <?php echo $userId; ?>;
        $('#bookings').DataTable({
            "order": [[0, "desc"]],
            "pageLength": 50
        });
        $(".update-btn").on("click", function () {
//            alert("foo");
            var name = $(".name").val();
            var balance = $(".balance").val().replace(/,/g, '.');
//            alert(balance);
            var data = JSON.stringify({'userId': userId, 'name': name, 'balance': balance});
            $.ajaxSetup({cache: false});
            $.ajax({
                type: "POST",
                url: "../asyncActors/updateUser.ajx.php",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: data,
                success: function (jsonReturn) {
//                    console.log(jsonReturn);
                    $("#check-overlay").show();
                    window.setTimeout(location.href = 'index.php?page=details&userId=' + userId, 2500);

                },
                error: function (xhr, status, errorThrown) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                }
            });
        });
        $(document).on('click', '.delete-booking', function () {
            var cancel = confirm("Diese Buchung wirklich löschen?");
            if (cancel) {
                var bookingId = $(this).attr("id");
                var data = JSON.stringify({'bookingId': bookingId});
                $.ajaxSetup({cache: false});
                $.ajax({
                    type: "POST",
                    url: "../asyncActors/deleteBooking.ajx.php",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: data,
                    success: function (jsonReturn) {
//                    console.log(jsonReturn);
                        $("#check-overlay").show();
                        window.setTimeout(location.href = 'index.php?page=details&userId=' + userId, 2500);
                    },
                    error: function (xhr, status, errorThrown) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                    }
                });
            }
        });
    });
</script>