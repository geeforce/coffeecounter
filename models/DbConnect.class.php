<?php
//db connection class using singleton pattern
class DbConnect {

    private $database = "coffeecounter";
    private $dbHost = "localhost";
    private $dbUser = "geeforce";
    private $dbPass = "1q2w3e4r5t";
    //variable to hold connection object.
    protected static $db;

    private function __construct() {

        try {
            self::$db = new PDO("mysql:host=$this->dbHost;dbname=$this->database", $this->dbUser, $this->dbPass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection Error: " . $e->getMessage();
        }
    }

    public static function getConnection() {
        if (!self::$db) {
            new DbConnect();
        }
        return self::$db;
    }

}

?>