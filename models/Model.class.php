<?php

class Model {

    static function find($id) {
        $className = get_called_class();
        $table = strtolower($className) . "s";
        try {
            $DBH = \DbConnect::getConnection();
            $qry = "SELECT * FROM " . $table . " WHERE id = ?";
            $result = $DBH->prepare($qry);
            $result->bindParam(1, $id);
            $result->execute();
            $res = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $className);
            if (count($res) != 0) {
                return $res[0];
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return $ex;
        }
    }

    /**
     * Description fetch
     *
     * @author guido
     * 
     * @params 2: field "=" value, 3 = field, operator, value
     * 
     */
    static function where($params, $append = "") {
        $className = get_called_class();
        $table = strtolower($className) . "s";
        $numParams = count($params);
        if ($numParams == 3) {
            $operator = $params[1];
            $val = $params[2];
        } else {
            $operator = "=";
            $val = $params[1];
        }
        $qry = "SELECT * FROM " . $table . " WHERE " . $params[0] . " " . $operator . " ? " . $append;
        try {
            $DBH = DbConnect::getConnection();
            $result = $DBH->prepare($qry);
            $result->bindParam(1, $val);
            $result->execute();
            $res = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $className);
            if (count($res) != 0) {
                return $res;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return $ex;
        }
    }

    static function delete($object) {
        if (gettype($object) == "object") {
            $table = strtolower(get_called_class()) . "s";
            $id = $object->GetId();
            $qry = "DELETE FROM " . $table . " WHERE id = ?";
            try {
                $DBH = DbConnect::getConnection();
                $result = $DBH->prepare($qry);
                $result->bindParam(1, $id);
                $result->execute();
            } catch (Exception $ex) {
                return $ex;
            }
        }
    }

    static function save($object) {
        if (gettype($object) == "object") {
            $className = get_called_class();
            $table = strtolower($className) . "s";
            $DBH = DbConnect::getConnection();
            $id = $object->GetId();
            if ($id) {
                // update
                $qry = "UPDATE " . $table . " SET ";
                foreach ($object as $key => $value) {
                    if ($key != "id") {
                        $paramBricks[] = $key . "=?";
                        $data[] = $value;
                    }
                }
                $data[] = $id;
                $params = implode(", ", $paramBricks);
                $qry .= $params . " WHERE id = ?";
                try {
                    $result = $DBH->prepare($qry);
                    $result->execute($data);
                } catch (Exception $ex) {
                    $fp = fopen('../error.log', 'a');
                    fwrite($fp, $ex);
                }
            } else {
                //insert
                $qry = " INSERT INTO " . $table . " (";
                foreach ($object as $key => $value) {
                    if ($key != "id") {
                        $paramBricks[] = $key;
                        $data[] = $value;
                        $qmBricks[] = "?";
                    }
                }
                $params = implode(", ", $paramBricks);
                $qm = implode(",", $qmBricks);
                $qry .= $params . ") VALUES (" . $qm . ")";
//                arrayOut($qry);
                try {
                    $result = $DBH->prepare($qry);
                    $result->execute($data);
                    $insertedId = $DBH->lastInsertId();
                    $object->setId($insertedId);
                } catch (Exception $ex) {
                    $fp = fopen('../error.log', 'a');
                    fwrite($fp, $ex);
                }
            }
            return $object;
        } else {
            echo "no object passed";
            return false;
        }
    }

    static function all() {
        $className = get_called_class();
        $table = strtolower($className) . "s";
        try {
            $DBH = DbConnect::getConnection();
            $qry = "SELECT * FROM " . $table;
            $result = $DBH->prepare($qry);
            $result->execute();
            $res = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $className);
            return $res;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    static function query($query, $createObj = FALSE) {
        $className = get_called_class();
        $table = strtolower($className) . "s";
        try {
            $DBH = DbConnect::getConnection();
            $result = $DBH->prepare($query);
            $result->execute();
            If ($createObj) {
                $res = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $className);
                return $res;
            }else{
                $res = $result->fetch(PDO::FETCH_ASSOC);
                return $res;
            }
        } catch (Exception $ex) {
            return $ex;
        }
        return true;
    }

}
