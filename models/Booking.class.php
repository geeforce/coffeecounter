<?php

class Booking extends Model {

    protected $id;
    protected $user_id;
    protected $date_time;
    protected $amount;

    function __construct($id = NULL, $user_id = NULL, $date_time = NULL, $amount = NULL) {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->date_time = $date_time;
        $this->amount = $amount;
    }

    function getId() {
        return $this->id;
    }

    function getUser_id() {
        return $this->user_id;
    }

    function getDate_time() {
        return $this->date_time;
    }

    function getAmount() {
        return $this->amount;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    function setDate_time($date_time) {
        $this->date_time = $date_time;
    }

    function setAmount($amount) {
        $this->amount = $amount;
    }

}
