<?php

class User extends Model {

    protected $id;
    protected $tag_id;
    protected $name;
    protected $visible;

    function __construct($id = NULL, $tag_id = NULL, $name = NULL, $visible = NULL) {
        $this->id = $id;
        $this->tag_id = $tag_id;
        $this->name = $name;
        $this->visible = $visible;
    }

    function getId() {
        return $this->id;
    }

    function getTag_id() {
        return $this->tag_id;
    }

    function getName() {
        return $this->name;
    }

    function getVisible() {
        return $this->visible;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTag_id($tag_id) {
        $this->tag_id = $tag_id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setVisible($visible) {
        $this->visible = $visible;
    }
    
    ##########################################################################
     function sumBookings() {
        $DBH = \DbConnect::getConnection();
        $qry = "SELECT SUM(amount)AS sum FROM bookings WHERE user_id = " . $this->id;
        $result = $DBH->prepare($qry);
        $result->execute();
        $res = round($result->fetch(PDO::FETCH_ASSOC)["sum"],4);
        return number_format($res,2);
    }

}
