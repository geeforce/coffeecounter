<?php

class Setting extends Model {

    protected $id;
    protected $price;
    function __construct($id = NULL, $price = NULL) {
        $this->id = $id;
        $this->price = $price;
    }
    function getId() {
        return $this->id;
    }

    function getPrice() {
        return $this->price;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPrice($price) {
        $this->price = $price;
    }



}
