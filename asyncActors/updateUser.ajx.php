<?php
header('Content-Type: application/json');
include_once '../pageheader.php';
$postJson = file_get_contents("php://input");
$postArr = json_decode($postJson, true);
//arrayOut($postArr);
$userId = $postArr['userId'];
$name = trim($postArr['name']);
$balance = $postArr['balance'];

if(!empty($name)){
    $user = User::find($userId);
    $user->setName($name);
    User::save($user);
}

if(!empty($balance)){
    str_replace(',', '.', $balance);
    settype($balance, "float");
    $dateTime = dbTimestamp();
    $inputBalance = new Booking(NULL, $userId, $dateTime , $balance);
    Booking::save($inputBalance);
}
echo json_encode(array('balance' => $balance));
