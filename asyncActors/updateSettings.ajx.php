<?php
header('Content-Type: application/json');
include_once '../pageheader.php';
$postJson = file_get_contents("php://input");
$postArr = json_decode($postJson, true);
//arrayOut($postArr);
$price = $postArr['price'];
$settings = Setting::find(1);
str_replace(',', '.', $price);
settype($price, "float");
$settings->setPrice($price);
Setting::save($settings);
echo json_encode(array('settings' => 'updated'));
