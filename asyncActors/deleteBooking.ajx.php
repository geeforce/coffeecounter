<?php
header('Content-Type: application/json');
include_once '../pageheader.php';
$postJson = file_get_contents("php://input");
$postArr = json_decode($postJson, true);
//arrayOut($postArr);
$bookingId = $postArr['bookingId'];
$bookingObj = new Booking($bookingId);
Booking::delete($bookingObj);
echo json_encode(array('bookingId' => $bookingId));