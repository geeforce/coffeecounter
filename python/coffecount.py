#!/usr/bin/env python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import MFRC522
import signal
import time
import pysql
import lcddriver
import traceback
import myfunctions as mf

display = lcddriver.lcd()
continue_reading = True
mf.beep()

while True:
	try:
		overallCoffee = pysql.overallCoffee()[0]
		break
	except Exception as e:
		mf.pyLogger(str(traceback.format_exc()))
		mf.lcdOut("No connection to DB.")
		time.sleep(10)

display.lcd_display_string("JZD Coffeecounter", 2)
display.lcd_display_string("Coffee counts: "+str(overallCoffee), 3)

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
	print "Coffeecounter stopped"
	display.lcd_clear()
	display.lcd_display_string("Coffeecounter", 2)
	display.lcd_display_string("stopped", 3)
	GPIO.cleanup()
	global continue_reading
	continue_reading = False

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()
loopcount = 0
#display.lcd_clear()
# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:
	loopcount = loopcount +1
	try:
		# Scan for cards
		#(status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
		(status,TagType,CardTypeRec) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
	except Exception as e:
		mf.pyLogger("CARD READING EXCEPTION: "+str(traceback.format_exc()))
		continue
	    
	# Get the UID of the card
 	#(status,uid) = MIFAREReader.MFRC522_Anticoll()
 	(status,uid,uidData) = MIFAREReader.MFRC522_Anticoll()
	try:	    
	    # If we have the UID, continue
		if status == MIFAREReader.MI_OK:
			mf.beep()
			display.lcd_clear()
			tId = ''
			for idBrick in uid:
				tId += str(idBrick)

			tagId = tId[:-4]
			print tagId
			userName = None
			user = pysql.getUser(tagId)
			if user == None:
			        userId = pysql.insertUser(tagId)
			        print "user inserted, id = "+str(tagId)
			else:
			        userId = int(user[0])
				if not (user[2] is None): 
			        	userName = user[2]
			        print(str(userId)+" - vorhandene ID ")

			#get price
			settings = pysql.getSettings()
			price = settings[1]

			#insert booking
			booking = pysql.insertBooking(userId,price)

			#get overall balance
			balance = round(pysql.getBalance(userId)[0],2)
			print("balance : " + str(balance))
			if userName == None:
				capt = "Tag-ID: "
				nameStr = tagId
			else:
				capt = "Name: "
				nameStr = userName

			display.lcd_clear()
			display.lcd_display_string(capt, 1)
			display.lcd_display_string(nameStr, 2)
			display.lcd_display_string("Balance: "+str(balance)+" Euro", 3)
			userName = None

			time.sleep(3)

			display.lcd_clear()
			display.lcd_display_string("JZD Coffeecounter", 2)
			overallCoffee = pysql.overallCoffee()[0]
			display.lcd_display_string("Coffee counts: "+str(overallCoffee), 3)
	except Exception as e:
		mf.pyLogger("MAIN LOOP EXCEPTION: "+str(traceback.format_exc()))
		continue
