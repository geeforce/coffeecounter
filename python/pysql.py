#!/usr/bin/python
import MySQLdb
import sys
import time
import traceback
import myfunctions as mf
mf.pyLogger("INFO: System Start")

def dbConnect():
	db = MySQLdb.connect(host="localhost", user="geeforce", passwd="1q2w3e4r5t", db="coffeecounter") 
	return db

while True:
	try:
		db = dbConnect()
		break
	except Exception as e:
 		mf.pyLogger(str(traceback.format_exc()))
		time.sleep(5)


def getUser(id):
	try:
		db = dbConnect()
		cur = db.cursor()
		qry = "SELECT * FROM users WHERE tag_id = '"+id+"'"
		cur.execute(qry)
		db.close()
		return cur.fetchone()
	except Exception as e:
		mf.pyLogger(str(traceback.format_exc()))
	
	
def insertUser(id):
	try:
		db = dbConnect()
		cur = db.cursor()
		qry = "INSERT INTO users (tag_id) VALUES('"+id+"')"
		cur.execute(qry)
		db.commit()
		db.close()
		return cur.lastrowid
	except Exception as e:
		mf.pyLogger(str(traceback.format_exc()))
	

def getSettings():
	try:
		db = dbConnect()
		cur = db.cursor()
		qry = "SELECT * FROM settings"
		cur.execute(qry)
		db.close()
		return cur.fetchone()
	except Exception as e:
		mf.pyLogger(str(traceback.format_exc()))
	

def insertBooking(userId,price):
	try:
		db = dbConnect()
		cur = db.cursor()
		qry = "INSERT INTO bookings(user_id,amount) VALUES(%s,%s)"
		val = (userId,price*-1)
		cur.execute(qry,val)
		db.commit()
		db.close()
		return cur.lastrowid	
	except Exception as e:
		mf.pyLogger(str(traceback.format_exc()))


def getBalance(userId):
	try:
		db = dbConnect()
		cur = db.cursor()
		qry = "SELECT SUM(amount)AS sum FROM bookings WHERE user_id = %s"
		cur.execute(qry,[userId])
		db.close()
		return cur.fetchone()	
	except Exception as e:
		mf.pyLogger(str(traceback.format_exc()))	
	

def overallCoffee():	
	try:
		db = dbConnect()
		cur = db.cursor()
		qry =  "SELECT COUNT(*)AS sum FROM bookings WHERE amount < 0"
		cur.execute(qry)
		db.close()
		return cur.fetchone()
	except Exception as e:
		mf.pyLogger(str(traceback.format_exc()))