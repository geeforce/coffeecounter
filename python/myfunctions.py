#!/usr/bin/python
# coding=utf8
import time
from datetime import datetime
import sys
import lcddriver
import math
import RPi.GPIO as GPIO

def pyLogger(message, writeOut = True):
	print(message)
	now = datetime.now()
	dt = now.strftime("%d.%m.%Y %H:%M:%S")
	f = open("/var/www/coffeecounter/python/python.log", "a")
	f.write(dt+" - " + message+"\n")
	f.close()
	if writeOut == True:
		lcdOut(message)

def lcdOut(message = ''):
	display = lcddriver.lcd()
	lineLength = 20
	if len(message) > lineLength:
		bricks = [message[i:i+lineLength] for i in range(0, len(message), lineLength)]
		numLines = len(bricks);
		#print(bricks)
		currentLine = 1
		#if numLines <= 4:
		if True:
			for brick in bricks:
				lcdBrick = brick.strip()
				display.lcd_display_string(lcdBrick, currentLine)
				if currentLine == 4:
					time.sleep(5)
					display.lcd_clear()
					currentLine = 1
					continue
				currentLine+=1

def beep(duration = 0.3):
	#buzzerPin = 12
	buzzerPin = 32
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(buzzerPin, GPIO.OUT)
	GPIO.output(buzzerPin, False)
	GPIO.output(buzzerPin, True)
	time.sleep(duration)
	GPIO.output(buzzerPin, False)
	#GPIO.cleanup()
