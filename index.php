<?php
session_start();
include_once 'pageheader.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Coffeecounter</title>
        <script src="./js/jquery.3.4.1.js"></script>
        <script src="./js/jquery-ui.js"></script>
        <script src="./js/bootstrap.bundle.min.js"></script>
        <script src="./js/datatables.min.js"></script>
        <script src="./js/fa-all.min.js"></script>

        <link rel="stylesheet" href="./css/jquery-ui.min.css">
        <link rel="stylesheet" href="./css/jquery-ui.structure.min.css">
        <link rel="stylesheet" href="./css/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="./css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/datatables.min.css">
        <link rel="stylesheet" href="./css/fa-all.min.css">
        <link rel="stylesheet" href="./css/custom.css">
        <link rel="icon" href="./img/coffeecup_fav.png">
        <script>
            $(document).ready(function () {

            });
        </script>
    </head>
    <body>
        <div id="check-overlay">
            <span style="font-size: 13em;">
                <span style="color: greenyellow;">
                    <i class="fa fa-check"></i>
                </span>
            </span>
        </div> 

        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="index.php"><img src="img/coffeecup.png" class="mr-3">Coffeecounter</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">List</a>
                    </li>
<!--                    <li class="nav-item">
                        <a class="nav-link" href="#">Names</a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=settings">Settings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?page=system">System Info</a>
                    </li>
                </ul>
            </div>
        </nav>

        <main role="main" class="container mt-5 pt-5">          
            <?php
            if (@$_REQUEST['page']) {
                if (file_exists('./pages/' . strip_tags($_REQUEST['page']) . '.php')) {
                    include_once './pages/' . strip_tags($_REQUEST['page']) . '.php';
                } else {
                    include_once './pages/listAll.php';
                }
            } else {
                include_once './pages/listAll.php';
            }
            ?>
        </main>
    </body>
</html>

